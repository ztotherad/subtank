#!/bin/bash
rm .version
# Bash Color
green='\033[01;32m'
red='\033[01;31m'
blink_red='\033[05;31m'
restore='\033[0m'

clear

# Resources
THREAD="-j$(grep -c ^processor /proc/cpuinfo)"
KERNEL="zImage"
DTBIMAGE="dtb"
DEFCONFIG="subtank_defconfig"

# Kernel Details
BASE_AK_VER="subtank"
VER=".A1.Shamu.uber"
AK_VER="$BASE_AK_VER$VER"

# Variables
export LOCALVERSION=~`echo $AK_VER`
export CROSS_COMPILE=${HOME}/toolchains/uber/arm-eabi-5.2/bin/arm-eabi-
export ARCH=arm
export SUBARCH=arm
export KBUILD_BUILD_USER=ztotherad
export KBUILD_BUILD_HOST=buildb0x

# Paths
KERNEL_DIR=`pwd`
ZIP_MOVE="${HOME}/kernels/release"
ZIMAGE_DIR="${HOME}/kernels/subtank/arch/arm/boot"
ANYKERNEL_DIR="${HOME}/kernels/subtank/out/kernel"

read -p "Clean working directory..(y/n)? : " achoice
	case "$achoice" in
		y|Y )
			rm -rf out/kernel/zImage
			make clean && make mrproper
			echo "Working directory cleaned...";;
		n|N )
	esac

function make_kernel {
		echo
		make $DEFCONFIG
		make $THREAD

}

function make_boot {
		cp -v $ZIMAGE_DIR/zImage-dtb ~/kernels/subtank/out/kernel/zImage

}

function make_zip {
		cd ~/kernels/subtank/out/
		zip -r9 `echo $AK_VER`.zip *
		mv  `echo $AK_VER`.zip $ZIP_MOVE
		cd $KERNEL_DIR
}


DATE_START=$(date +"%s")

echo "--------------"
echo "Kernel Version:"
echo "--------------"

echo -e "${red}"; echo -e "${blink_red}"; echo "$AK_VER"; echo -e "${restore}";

echo -e "${green}"
echo "-----------------"
echo "Making Subtank Kernel:"
echo "-----------------"
echo -e "${restore}"

while read -p "Do you want to build kernel (y/n)? " dchoice
do 
case "$dchoice" in
	y|Y)
		make_kernel
		make_boot
		make_zip
		break
		;;
	n|N )
		break
		;;
	* )
		echo 
		echo "Invalid try again!"
		echo
		;;	
esac
done

echo -e "${green}"
echo "------------"
echo "Build Completed in:"
echo "------------"
echo -e "${restore}"

DATE_END=$(date +"%s")
DIFF=$(($DATE_END - $DATE_START))
echo "Time: $(($DIFF / 60)) minute(s) and $(($DIFF % 60))
seconds."
echo

